#!/bin/bash

# Potentially dangerous script: does rm -rf * 
# For extra safety: run with firejail --private=. <program>

ACCOUNT=project_2000403 # Modify the billable project here if needed
MODULES="intel/19.0.4 hpcx-mpi/2.4.0" # Modules used to build Pencil Code
BASE_DIR=$(pwd)

if [ ! -d $BASE_DIR/helical-MHDturb ];then
	echo "Could not find helical-MHDturb in $(pwd)."
	echo "Ensure you are in the cloned directory of https://bitbucket.org/jpekkila/pekkila-2021-artifacts/src/master/"
	exit 1
fi

git submodule update --init --recursive
if [[ ! -d pencil-code ]]
then
	echo "Could not find pencil-code in $(pwd)."
	echo "Ensure you have initialized submodules with 'git submodule update --init --recursive'"
	exit 1
fi

# Setup modules
module purge
module load $MODULES

# Setup Pencil Code
cd pencil-code
source ./sourceme.sh
cd samples

# Copy sample
cp -rf $BASE_DIR/helical-MHDturb .
cd helical-MHDturb
make cleann
rm -rf data/*
mkdir data
cd ..

# Create test dirs
ngrid=520
nghost=3
procs_per_node=40
node_counts=(1 2 4 8 16 )
nprocx_counts=(2 2 2 2 2 )
nprocy_counts=(10 20 40 40 40 )
nprocz_counts=(2 2 2 4 8 )

# Check that the decomposition is valid
for i in "${!node_counts[@]}"
do
	nodes=${node_counts[$i]}
	let nprocs=$procs_per_node*$nodes
	nprocx=${nprocx_counts[$i]}
	nprocy=${nprocy_counts[$i]}
	nprocz=${nprocz_counts[$i]}
	let nprocxyz=$nprocx*$nprocy*$nprocz
	if [ $nprocs != $nprocxyz ]
	then
		echo "FATAL ERROR: Invalid decomposition! nprocs vs nprocxyz: " $nproc $nprocxyz
		exit 1
	fi

    if (( $ngrid % $nprocx ))
    then
        echo "FATAL ERROR: nprocx $nprocx not a multiple of ngrid $ngrid!"
        exit 1
    fi
    if (( $ngrid % $nprocy ))
    then
        echo "FATAL ERROR: nprocy $nprocy not a multiple of ngrid $ngrid!"
        exit 1
    fi
    if (( $ngrid % $nprocz ))
    then
        echo "FATAL ERROR: nprocz $nprocz not a multiple of ngrid $ngrid!"
        exit 1
    fi

    if (( $ngrid / $nprocx < $nghost ))
    then
        echo "FATAL ERROR: nprocx $nprocx too large, results in too small subgrid size."
        exit 1
    fi
    if (( $ngrid / $nprocy < $nghost ))
    then
        echo "FATAL ERROR: nprocy $nprocy too large, results in too small subgrid size."
        exit 1
    fi
    if (( $ngrid / $nprocz < $nghost ))
    then
        echo "FATAL ERROR: nprocz $nprocz too large, results in too small subgrid size."
        exit 1
    fi
done

# Run!
# Takes the index of the current test as a parameter
benchmark() {
	nodes=${node_counts[$1]}

	rm -rf helical-MHDturb-$nodes
	cp -rf helical-MHDturb helical-MHDturb-$nodes
	cd helical-MHDturb-$nodes

	let nprocs=$procs_per_node*$nodes
	nprocx=${nprocx_counts[$1]}
	nprocy=${nprocy_counts[$1]}
	nprocz=${nprocz_counts[$1]}
	
	echo "nprocs " $nprocs
	echo "ngrid " $ngrid
	echo "nprocs (" $nprocx ", " $nprocy ", " $nprocz ")"

	sed -i s/"ncpus=[0-9]*"/"ncpus=$nprocs"/ src/cparam.local
	sed -i s/"nprocx=[0-9]*"/"nprocx=$nprocx"/ src/cparam.local
	sed -i s/"nprocy=[0-9]*"/"nprocy=$nprocy"/ src/cparam.local
	sed -i s/"nprocz=[0-9]*"/"nprocz=$nprocz"/ src/cparam.local

	sed -i s/"nxgrid=[0-9]*"/"nxgrid=$ngrid"/ src/cparam.local

    partition=large
    if (( $nodes == 1 ))
    then
        partition=small
    fi

    mem_per_core=4G

	# Build and run PC
	pc_setupsrc
    	pc_build -f Intel_MPI FFLAGS+=-O2
    	# pc_build -f Intel_MPI FFLAGS+="-O3 -xHost -fp-model fast=2 -no-prec-div -fimf-use-svml=true -qopt-zmm-usage=high"
	start=$(sbatch -n $nprocs -N $nodes --time=00:30:00 --partition=$partition --account=$ACCOUNT --mem-per-cpu=$mem_per_core --exclusive ./start.csh)
	sbatch --dependency=afterok:${start##* } -n $nprocs -N $nodes --time=00:30:00 --partition=$partition --account=$ACCOUNT --mem-per-cpu=$mem_per_core --exclusive ./run.csh
	
	cd ..
}

for i in "${!node_counts[@]}"
do
    benchmark "$i" &
done
