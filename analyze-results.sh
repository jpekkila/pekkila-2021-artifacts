#!/bin/bash
node_counts=(1 2 4 8 16)

echo "nodes,time/timestep/meshpoint(us)"
for i in "${!node_counts[@]}"
do
    nodes=${node_counts[$i]}

    printf $nodes

    cat pencil-code/samples/helical-MHDturb-$nodes/slurm-* | grep "Wall clock time/timestep/meshpoint" | cut -d'=' -f2
done

echo ""
echo "With"
cat run-pc-scaling-tests.sh | grep "MODULES="
cat run-pc-scaling-tests.sh | grep "ngrid="
cat run-pc-scaling-tests.sh | grep "nprocx_counts="
cat run-pc-scaling-tests.sh | grep "nprocy_counts="
cat run-pc-scaling-tests.sh | grep "nprocz_counts="
