This sample was used to generate the CPU benchmarks for 
Pekkil� et al. "Scalable communication for high-order stencil computations using CUDA-aware MPI", 2021.

# Cloning
	- 'git clone https://jpekkila@bitbucket.org/jpekkila/pekkila-2021-artifacts.git'
	- 'git submodule update --init --recursive'

# Manually setting up the test case (DEPRECATED)
	- Clone Pencil Code from https://github.com/pencil-code/, commit '7ddde40e68c7885c494ba8f880a517ad37646be3'
	- Copy helical-MHDturb to pencil-code/samples, overwriting if necessary

# Building and running (single node)
	- module load intel/19.0.4 hpcx-mpi/2.4.0
	- cd pencil-code
	- source ./sourceme.sh
	- cd samples/helical-MHDturb
	- pc_setupsrc
	- pc_build -f Intel_MPI FFLAGS+=-O2
	- sbatch -n 40 -N 1 --account=<project id> ./start.csh
	- sbatch -n 40 -N 1 --account=<project id> ./run.csh

> The sample in helical-MHDturb is modified from a sample provided by Pencil Code
(C) NORDITA. Pencil Code is licenced under GNU General Public Licence V2. See
https://github.com/pencil-code/pencil-code/tree/master/license for more
details.
